package rybak.blogAttempt3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rybak.blogAttempt3.domain.Post;
import rybak.blogAttempt3.exaption.ResourceNotFoundException;
import rybak.blogAttempt3.repository.PostRepository;
import rybak.blogAttempt3.repository.UserRepository;

import javax.validation.Valid;

@RestController
public class PostController {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/{userId}/posts")
    public Page<Post> getAllPostsByUserId(@PathVariable("userId") Long userId, Pageable pageable) {
        return postRepository.findByUserId(userId,pageable);
    }

    @PostMapping("/user/{userId}/posts")
    public Post createPost(@PathVariable("userId") Long userId,
                           @Valid @RequestBody Post post) {
        return userRepository.findById(userId).map(user -> {
            post.setAuthor(user);
            return postRepository.save(post);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + userId + " not found"));
    }

    @PutMapping("/user/{userId}/posts/{postId}")
    public Post updatePost(@PathVariable("userId") Long userId,
                           @PathVariable("postId") Long postId, @Valid @RequestBody Post postRequest) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id" + userId + "not found");
        }
        return postRepository.findById(postId).map(post -> {
            post.setDescription(postRequest.getDescription());
            post.setContent(postRequest.getContent());
            post.setTitle(postRequest.getTitle());
            return postRepository.save(post);
        }).orElseThrow(() -> new ResourceNotFoundException("Post id " + postId + "not found"));
    }

    @DeleteMapping("/user/{userId}/posts/{postId}")
    public ResponseEntity<?> deletePost(@PathVariable("userId") Long userId,
                                        @PathVariable("postId") Long postId) {
        return postRepository.findByIdAndUserId(postId, userId).map(post -> {
            postRepository.delete(post);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Post not found with id" + postId + "and user id" + userId));
    }

}
