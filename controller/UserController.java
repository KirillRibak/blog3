package rybak.blogAttempt3.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import rybak.blogAttempt3.domain.User;
import rybak.blogAttempt3.exaption.ResourceNotFoundException;
import rybak.blogAttempt3.repository.UserRepository;

import javax.validation.Valid;

@RestController
@RequestMapping("user")
public class UserController {

    private UserRepository userRepo;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserRepository userRepo, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepo = userRepo;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@Valid @RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.save(user);
    }

    @Autowired
    public void setRepo(UserRepository repo) {
        this.userRepo = repo;
    }

    @GetMapping
    public Page<User> list(Pageable pageable) {
        return userRepo.findAllAndReturnPage(pageable);
    }

    @GetMapping("{id}")
    public User getOne(@PathVariable("id") User user) {
        if(!userRepo.existsById(user.getId())){
            throw new ResourceNotFoundException();
        }
        return userRepo.getOne(user.getId());
    }



    @PutMapping("{id}")
    public User update(@PathVariable("id") User userFromDb, @Valid @RequestBody User user) {
        BeanUtils.copyProperties(user, userFromDb, "id");
        return userRepo.save(userFromDb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") User user) {
        userRepo.delete(user);
    }
}
