package rybak.blogAttempt3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rybak.blogAttempt3.domain.Tag;

import java.util.UUID;

public interface TagRepository extends JpaRepository<Tag,Long> {
}
