package rybak.blogAttempt3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rybak.blogAttempt3.domain.Post;

import java.util.Optional;

public interface PostRepository extends JpaRepository<Post,Long> {
     @Query("select p from Post p where p.author.id = :userId ")
     Page<Post> findByUserId(@Param("userId") Long userId,Pageable pageable);

     @Query("select p from Post p where p.id = :id AND p.author.id = :userId")
     Optional<Post> findByIdAndUserId(@Param("id")Long id,@Param("userId") Long userId);

}