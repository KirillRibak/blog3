package rybak.blogAttempt3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import rybak.blogAttempt3.domain.User;

public interface UserRepository extends JpaRepository<User,Long> {
    @Query("select u from User u")
    Page<User>findAllAndReturnPage(Pageable pageable);

    User findByName(String name);

}
