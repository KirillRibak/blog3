package rybak.blogAttempt3.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rybak.blogAttempt3.domain.Comment;

import java.util.Optional;


public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("select c from Comment c where c.post.id = :postId and c.post.author.id= :userId  ")
    Page<Comment> findByUserIdAndPostId( @Param("userId") Long userId, @Param("postId")Long postId,Pageable pageable);

    @Query("select c from Comment c where c.id= :id and c.post.id = :postId and c.post.author.id= :userId ")
    Optional<Comment> findByIdAndPostIdAndUserId(@Param("id") Long id,@Param("postId")Long postId, @Param("userId")Long userId);
}
